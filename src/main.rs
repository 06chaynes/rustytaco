use rust_embed::RustEmbed;
use serenity::{
    async_trait,
    client::{Client, Context, EventHandler},
    framework::standard::{
        macros::{command, group},
        CommandResult, StandardFramework,
    },
    http::AttachmentType,
    model::channel::Message,
    Result as SerenityResult,
};
use dotenv;

#[derive(RustEmbed)]
#[folder = "assets/"]
struct Asset;

#[group]
#[commands(ping, taco)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {}

#[tokio::main]
async fn main() {
    let framework = StandardFramework::new()
        .configure(|c| c.prefix("~"))
        .group(&GENERAL_GROUP);

    dotenv::dotenv().ok();
    let token = dotenv::var("DISCORD_TOKEN").expect("token");
    let mut client = Client::builder(token)
        .event_handler(Handler)
        .framework(framework)
        .await
        .expect("Error creating client");

    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn ping(context: &Context, msg: &Message) -> CommandResult {
    check_msg(msg.channel_id.say(&context.http, "Pong!").await);
    Ok(())
}

#[command]
async fn taco(context: &Context, msg: &Message) -> CommandResult {
    let taco_gif = Asset::get("taco.gif").unwrap();
    check_msg(
        msg.channel_id
            .send_message(&context.http, |m| {
                m.content("Taco Time!");
                m.embed(|e| {
                    e.title("Eat More Tacos");
                    e.description("Fuck yeah, tacos");
                    e.image("attachment://taco.gif");
                    e.footer(|f| {
                        f.text("Taco Dance");
                        f
                    });
                    e
                });
                m.add_file(AttachmentType::from((taco_gif.as_ref(), "taco.gif")));
                m
            })
            .await,
    );
    Ok(())
}

fn check_msg(result: SerenityResult<Message>) {
    if let Err(why) = result {
        println!("Error sending message: {:?}", why);
    }
}
